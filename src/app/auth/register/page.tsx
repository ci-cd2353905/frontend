'use client';

import React, { useState } from "react";
import { register } from "@/service/auth.service";
import Link from 'next/link'
import router from "next/router";
import CommonUtils from "../../../utils/common.util";
import API from "../../../api/code.api";

// Define the interface for the form data
interface FormData {
    email: string;
    password: string;
    fullname: string;
    avatar: string;
    dob: Date;
}

// Define the component
export default function Register() {
    // Use state hooks to store the form data and the error message
    const [formData, setFormData] = useState<FormData>({
        email: '', password: '', fullname: '',
        avatar: '', dob: new Date()
    });
    const [error, setError] = useState<string>("");
    const [isRegisterSuccess, setisRegisterSuccess] = useState<Boolean>(false)

    // Handle the input change event
    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        // Update the form data state
        setFormData({
            ...formData,
            [e.target.name]: e.target.value,
        });
    };

    const handleOnChangeImg = async (event: any) => {
        let data = event.target.files;
        let file = data[0];
        if (file) {
            let base64 = await CommonUtils.getBase64(file);
            setFormData(prevFormData => ({
                ...prevFormData,
                avatar: base64 as string
            }));
        }
    }

    // Handle the form submit event
    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        // Prevent the default behavior of form submission
        e.preventDefault();
        // Clear the error message state
        setError("");
        try {
            const formDT = new FormData()
            formDT.append("avatar", formData.avatar)
            // Call the api with the form data
            const response = await register(formData.email, formData.password, formData.fullname, formData.avatar, formData.dob)
            // Check if the login is successful
            if (response.status.code === API.CREATED) {
                // Invoke the callback function with the email
                setFormData(response.data)
                setisRegisterSuccess(true);
                return router.push('/auth/login')
            } else {
                // Set the error message state with the api response
                setError(response.data.message);
            }
        } catch (err) {
            // Set the error message state with the network error
            throw err
        }
    };

    return (
        <div className="flex min-h-full flex-1 flex-col justify-center px-6 py-12 lg:px-8" >
            <div className="sm:mx-auto sm:w-full sm:max-w-sm">
                <img
                    className="mx-auto h-10 w-auto"
                    src="https://tailwindui.com/img/logos/mark.svg?color=indigo&shade=600"
                    alt="Your Company"
                />
                {isRegisterSuccess === true &&
                    <h2 className="mt-10 text-center text-2xl font-bold leading-9 tracking-tight text-gray-900">
                    </h2>
                }
            </div>
            <div className="mt-10 sm:mx-auto sm:w-full sm:max-w-sm">
                <form onSubmit={handleSubmit} className="space-y-6" encType="multipart/form-data">
                    <div>
                        <label htmlFor="email" className="block text-sm font-medium leading-6 text-gray-900">
                            Email address
                        </label>
                        <div className="mt-2">
                            <input
                                onChange={handleChange}
                                id="email"
                                name="email"
                                type="email"
                                autoComplete="email"
                                required
                                className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                            />
                        </div>
                    </div>

                    <div>
                        <div className="flex items-center justify-between">
                            <label htmlFor="password" className="block text-sm font-medium leading-6 text-gray-900">
                                Password
                            </label>
                            <div className="text-sm">
                                <a href="#" className="font-semibold text-indigo-600 hover:text-indigo-500">
                                    Forgot password?
                                </a>
                            </div>
                        </div>
                        <div className="mt-2">
                            <input
                                onChange={handleChange}
                                id="password"
                                name="password"
                                type="password"
                                autoComplete="current-password"
                                required
                                className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                            />
                        </div>
                    </div>

                    <div>
                        <div className="flex items-center justify-between">
                            <label htmlFor="text" className="block text-sm font-medium leading-6 text-gray-900">
                                Fullname
                            </label>
                        </div>
                        <div className="mt-2">
                            <input
                                onChange={handleChange}
                                id="fullname"
                                name="fullname"
                                type="fullname"
                                required
                                className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                            />
                        </div>
                    </div>

                    <div>
                        <div className="flex items-center justify-between">
                            <label htmlFor="text" className="block text-sm font-medium leading-6 text-gray-900">
                                Avatar
                            </label>
                        </div>
                        <div className="mt-2">
                            <input
                                onChange={handleOnChangeImg}
                                id="avatar"
                                name="avatar"
                                type="file"
                                className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                            />
                        </div>
                    </div>

                    <div>
                        <div className="flex items-center justify-between">
                            <label htmlFor="text" className="block text-sm font-medium leading-6 text-gray-900">
                                D.O.B
                            </label>
                        </div>
                        <div className="mt-2">
                            <input
                                onChange={handleChange}
                                id="dob"
                                name="dob"
                                type="date"
                                className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                            />
                        </div>
                    </div>

                    <div>
                        <button
                            type="submit"
                            className="flex w-full justify-center rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                        >
                            Sign in
                        </button>
                    </div>
                </form>

                <p className="mt-10 text-center text-sm text-gray-500">
                    Already have account ?{' '}
                    <Link href="/auth/login">
                        Login
                    </Link>
                </p>
            </div>
        </div>
    )
};
