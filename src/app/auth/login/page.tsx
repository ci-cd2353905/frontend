'use client';

import React, { useState } from "react";
import { login } from "@/service/auth.service";
import Link from 'next/link'
import { useRouter } from "next/router";


// Define the interface for the form data
interface FormData {
    username: string;
    password: string;
}

// Define the component
export default function Login() {
    // Use state hooks to store the form data and the error message
    const [formData, setFormData] = useState<FormData>({ username: '', password: '' });
    const [error, setError] = useState<string>("");
    const [isLoginSuccess, setLoginSuccess] = useState<Boolean>(false)
    const [isAuthenticated, setIsAuthenticated] = useState<Boolean>(false)
    // const router = useRouter()


    // Handle the input change event
    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        // Update the form data state
        setFormData({
            ...formData,
            [e.target.name]: e.target.value,
        });
    };

    // Handle the form submit event
    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        // Prevent the default behavior of form submission
        e.preventDefault();
        // Clear the error message state
        setError("");
        try {
            // Call the api with the form data
            const response = await login(formData.username, formData.password)
            // Check if the login is successful
            if (response.code === 200) {
                // Invoke the callback function with the emai
                setFormData(response.data);
                setIsAuthenticated(true);
                setLoginSuccess(true);
                // return router.push('/posts')
            } else {
                // Set the error message state with the api response
                setError(response.data);
            }
            return isAuthenticated
        } catch (err) {
            // Set the error message state with the network error
            throw err
        }
    };


    return (
        <div className="flex min-h-full flex-1 flex-col justify-center px-6 py-12 lg:px-8" >
            <div className="sm:mx-auto sm:w-full sm:max-w-sm">
                <img
                    className="mx-auto h-10 w-auto"
                    src="https://tailwindui.com/img/logos/mark.svg?color=indigo&shade=600"
                    alt="Your Company"
                />
                {isLoginSuccess === true &&
                    <h2 className="mt-10 text-center text-2xl font-bold leading-9 tracking-tight text-gray-900">
                        { }
                    </h2>
                }
            </div>
            <div className="mt-10 sm:mx-auto sm:w-full sm:max-w-sm">
                <form onSubmit={handleSubmit} className="space-y-6" action="#" method="POST">
                    <div>
                        <label htmlFor="username" className="block text-sm font-medium leading-6 text-gray-900">
                            Username
                        </label>
                        <div className="mt-2">
                            <input
                                onChange={handleChange}
                                id="username"
                                name="username"
                                type="username"
                                autoComplete="username"
                                required
                                className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                            />
                        </div>
                    </div>

                    <div>
                        <div className="flex items-center justify-between">
                            <label htmlFor="password" className="block text-sm font-medium leading-6 text-gray-900">
                                Password
                            </label>
                            <div className="text-sm">
                                <a href="#" className="font-semibold text-indigo-600 hover:text-indigo-500">
                                    Forgot password?
                                </a>
                            </div>
                        </div>
                        <div className="mt-2">
                            <input
                                onChange={handleChange}
                                id="password"
                                name="password"
                                type="password"
                                autoComplete="current-password"
                                required
                                className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                            />
                        </div>
                    </div>

                    <div>
                        <button
                            type="submit"
                            className="flex w-full justify-center rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                        >
                            Sign in
                        </button>
                    </div>
                </form>

                <p className="mt-10 text-center text-sm text-gray-500">
                    Not a member?{' '}
                    <Link href="/auth/register">
                        Register
                    </Link>
                </p>
            </div>
        </div>
    )
};
