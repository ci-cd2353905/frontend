import Api from "../api/auth.api";
import { instance } from "../api/instance.api";
import Cookies from 'js-cookie';

const login = (username: any, password: any) => {
    console.log("🚀 ~ login ~ password:", password)
    console.log("🚀 ~ login ~ email:", username)
    return instance
        .post(`${Api.LOGIN_URL}`, {
            username,
            password,
        })
        .then((response: any) => {
            console.log("🚀 ~ .then ~ response:", response)
            const { data: {
                data,
                accessToken
            }
            } = response
            const isSuccess = !!(data && accessToken)
            console.log(response)
            if (isSuccess) {
                Cookies.set("user", JSON.stringify(data));
                Cookies.set('accessToken', accessToken)
                process.env.ACCESS_TOKEN = accessToken
            }

            return response.data;
        });
};

const register = async (email: string, password: string, fullname: string, avatar: string, dob: Date) => {
    try {
        const response = await instance.post(`${Api.REGISTER_URL}`, {
            email,
            password,
            fullname,
            avatar,
            dob
        });
        return response.data;
    } catch (error) {
        // Xử lý lỗi và trả về thông báo lỗi cụ thể
        throw new Error("Đã xảy ra lỗi khi đăng ký");
    }
}



const getCurrentUser = (): any => {
    const isServer = typeof window === "undefined";
    if (!isServer) {
        return JSON.parse(<string>localStorage.getItem("user"))
    }
}

const logout = () => {
    localStorage.removeItem("user");
};

export { login, register, getCurrentUser, logout }