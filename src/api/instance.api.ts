import axios, { AxiosInstance } from 'axios'
import Api from "../api/auth.api";



const getLocalStorage = (key: string) => {
    const isServer = typeof window === "undefined";
    if (!isServer) {
        return localStorage.getItem(key)
    }
}


const instance: AxiosInstance = axios.create({
    baseURL: `${Api.URL}`,
    headers: {
        Authorization: `${getLocalStorage("accessToken")}`
    }
})


instance.interceptors.response.use(
    response => response,
    error => {
        console.error(error)
    });
export { instance, getLocalStorage }