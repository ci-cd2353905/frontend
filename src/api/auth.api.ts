class Api {
    public static readonly URL = 'http://localhost:3001'
    public static readonly LOGIN_URL = 'http://localhost:3001/api/users/login'
    public static readonly REGISTER_URL = 'http://localhost:3001/api/users/register'
}

export default Api